package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;

public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getServerPortStr();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
