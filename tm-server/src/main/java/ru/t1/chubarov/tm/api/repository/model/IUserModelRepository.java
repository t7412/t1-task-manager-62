package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.User;

import java.util.List;

@Repository
@Scope("prototype")
public interface IUserModelRepository extends IModelRepository<User> {

    @Nullable
    User findByLogin(@NotNull String id);

    @Nullable
    User findByEmail(@NotNull String id);

    @NotNull
    @Query("SELECT p FROM User p ")
    List<User> findAll();

    @Nullable
    User findFirstById(@NotNull String id);

    void deleteAll();

    void deleteById(@Nullable String Id);

    @Query("DELETE FROM User p WHERE p.id = :id")
    void remove(@NotNull @Param("id") String id);

    long count();

    @NotNull
    Boolean existsByLogin(@NotNull String login);

    @NotNull
    Boolean existsByEmail(@NotNull String email);

}
