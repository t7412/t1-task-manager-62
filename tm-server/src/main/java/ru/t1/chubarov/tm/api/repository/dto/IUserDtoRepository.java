package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.dto.model.UserDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface IUserDtoRepository extends IDtoRepository<UserDTO>{

    @Nullable
    UserDTO findByLogin(@NotNull String id);

    @Nullable
    UserDTO findByEmail(@NotNull String id);

    @NotNull
    @Query("SELECT p FROM UserDTO p ")
    List<UserDTO> findAll();

    @NotNull
    @Query("SELECT p FROM UserDTO p WHERE p.id = :id")
    UserDTO findOneById(@Nullable @Param("id") String id);

    @Nullable
    UserDTO findFirstById(@NotNull String id);

    void deleteAll();

    void deleteById(@Nullable String Id);

    @Query("DELETE FROM UserDTO p WHERE p.id = :id")
    void remove(@NotNull @Param("id") String id);

    long count();

    @NotNull
    Boolean existsByLogin(@NotNull String login);

    @NotNull
    Boolean existsByEmail(@NotNull String email);

}
