package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateResponse extends AbstractProjectResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectCreateResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

}
