package ru.t1.chubarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);
        @NotNull final TaskDTO task = taskEndpoint.getTaskById(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by id.";
    }

}
