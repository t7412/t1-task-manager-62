package ru.t1.chubarov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chubarov.tm.api.service.ITokenService;
import ru.t1.chubarov.tm.dto.request.UserLoginRequest;
import ru.t1.chubarov.tm.dto.request.UserLogoutRequest;
import ru.t1.chubarov.tm.dto.request.UserProfileRequest;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.marker.SoapCategory;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.service.TokenService;

@Category(SoapCategory.class)
public class SessionEndpointTest {

    @Nullable
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final ITokenService tokenService = new TokenService();

    @Nullable
    private String token;

    @Test
    public void testLogin() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        Assert.assertNotNull(token);
    }

    @Test
    public void testLogout() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        Assert.assertNotNull(token);
        token = authEndpoint.logout(new UserLogoutRequest(token)).getToken();
        Assert.assertNull(token);
    }

    @Test
    public void testViewProfile() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        Assert.assertNotNull(token);
        @NotNull final UserDTO user = authEndpoint.profile(new UserProfileRequest(token)).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("admin", user.getLogin());
        Assert.assertEquals(Role.ADMIN, user.getRole());
        Assert.assertEquals("AdminMiddleName", user.getMiddleName());
        Assert.assertEquals("AdminFirstName", user.getFirstName());
        Assert.assertEquals("AdminLastName", user.getLastName());
    }

}
